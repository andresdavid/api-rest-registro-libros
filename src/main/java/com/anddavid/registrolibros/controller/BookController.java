package com.anddavid.registrolibros.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anddavid.registrolibros.entity.Book;
import com.anddavid.registrolibros.services.BookService;

@RestController
@RequestMapping("/api/v1")
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	@GetMapping(value="/books")
	public ResponseEntity<Object> get(){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Book> list = bookService.findAll();
			return new ResponseEntity<Object>(list,HttpStatus.OK);
		}
		catch (Exception e) {
			map.put("message", e.getMessage());
			return new ResponseEntity<>( map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value="/books/{id}")
	public ResponseEntity<Object> getById(@PathVariable Long id){
		try {
			Book data = bookService.findById(id);
			return new ResponseEntity<Object>(data,HttpStatus.OK);
		}
		catch (Exception e){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("message", e.getMessage());
			return new ResponseEntity<>( map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value="/books")
	public ResponseEntity<Object> create(@RequestBody Book book){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Book res = bookService.save(book);
			return new ResponseEntity<Object>(res,HttpStatus.OK);
		}
		catch (Exception e) {
			map.put("message", e.getMessage());
			return new ResponseEntity<>( map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/books/{id}")
	public ResponseEntity<Object> update(@RequestBody Book book, @PathVariable Long id){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Book currentBook = bookService.findById(id);
			
			currentBook.setTitle(book.getTitle());
			currentBook.setAuthor(book.getAuthor());
			currentBook.setDate(book.getDate());
			
			Book res = bookService.save(book);
			
			return new ResponseEntity<Object>(res,HttpStatus.OK);
		}
		catch (Exception e) {
			map.put("message", e.getMessage());
			return new ResponseEntity<>( map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/books/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Book currentBook = bookService.findById(id);
			bookService.delete(currentBook);
			map.put("deleted", true);
			return new ResponseEntity<Object>(map,HttpStatus.OK);
		}
		catch (Exception e) {
			map.put("message", e.getMessage());
			return new ResponseEntity<>( map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
