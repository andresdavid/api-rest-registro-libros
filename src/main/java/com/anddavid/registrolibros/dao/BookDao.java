package com.anddavid.registrolibros.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.anddavid.registrolibros.entity.Book;

@Repository
public interface BookDao extends JpaRepository<Book, Long> {

}
