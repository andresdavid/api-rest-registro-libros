package com.anddavid.registrolibros.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anddavid.registrolibros.dao.BookDao;
import com.anddavid.registrolibros.entity.Book;

import jakarta.transaction.Transactional;

@Service
public class BookServiceImplement implements BookService {
	
	@Autowired
	private BookDao bookDao;

	@Override
	@Transactional
	public List<Book> findAll() {
		return (List<Book>) bookDao.findAll();
	}

	@Override
	@Transactional
	public Book save(Book book) {
		return bookDao.save(book);
	}

	@Override
	@Transactional
	public Book findById(Long id) {
		return bookDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Book book) {
		bookDao.delete(book);	
	}

}
