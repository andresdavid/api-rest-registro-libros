package com.anddavid.registrolibros.services;

import java.util.List;

import com.anddavid.registrolibros.entity.Book;

public interface BookService {
	
	public List<Book> findAll();
	
	public Book save(Book book);
	
	public Book findById(Long id);
	
	public void delete(Book book);

}
